<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ajout confirmé</title>
<link rel="stylesheet"
		href="style.css">
<body>

<?php
	
try{
	date_default_timezone_set('Europe/Paris');
	
	echo "<h1>Votre film a bien ete ajoute</h1>";
	$file_db=new PDO('sqlite:film.sqlite3');
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
	
	
	$statement=$file_db->prepare('INSERT INTO film values((select max(id)+1 from film),:Movie, :Real, (select max(idr)+1 from film) ,:Year, :Type)');
	$statement->bindParam(':Movie',$_GET['Movie']);
	$statement->bindParam(':Real',$_GET['Real']);
	$statement->bindParam(':Year',$_GET['Year']);
	$statement->bindParam(':Type',$_GET['Type']);
	$statement->execute();
	
	echo "<div>Film : ".$_GET['Movie']."\n Realisateur : ".$_GET['Real']."\n Genre : ".$_GET['Type']."\n Annee : ".$_GET['Year']."</div>";
	
	
}catch(PDOException $ex){
	  echo $ex->getMessage();
	  }
?>

</body>
</head>
</html>
