<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Reponse a la demande</title>
<link rel="stylesheet"
		href="style.css">
<body>
	
<?php
	
try{
	date_default_timezone_set('Europe/Paris');
   
	echo "<h1>Voici votre requete</h1>";
	echo "<div>"."Les films de type ".$_GET['Genre']." sont :"."</div>";
	$file_db=new PDO('sqlite:film.sqlite3');
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
	
	
	$statement=$file_db->prepare('Select * from film where genre=:genre');
	$statement->bindParam(':genre',$_GET['Genre']);
	$statement->execute();
	
	echo "<ul>";
	while($result=$statement->fetch()){
		echo "<div>"."<li>".$result['NomFilm'].' '
		.$result['annee']."</li>\n"."</div>";
		echo "<div><img src=image/".$result['NomFilm'].".jpg></div> ";
	}
	echo "</ul>";
	$file_db=null;
}
catch(PDOException $ex){
	  echo $ex->getMessage();
	  }
	
?>	 
</body>
</head>
</html>
