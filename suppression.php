<html>
<head>
<meta charset="utf-8">
<title>Ajout confirmé</title>
<link rel="stylesheet"
		href="style.css">
<body>

<?php
	
try{
	date_default_timezone_set('Europe/Paris');
	
	echo "<h1>Voici le film que vous avez supprime</h1>";
	$file_db=new PDO('sqlite:film.sqlite3');
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
	
	
	$statement=$file_db->prepare('Delete from film where NomFilm==:Movie');
	$statement->bindParam(':Movie',$_GET['Movie']);
	$statement->execute();
	
	echo "<div>Film : ".$_GET['Movie']."</div>";
	
	
}catch(PDOException $ex){
	  echo $ex->getMessage();
	  }
?>

</body>
</head>
</html>
